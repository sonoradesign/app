import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Page1 } from '../pages/page1/page1';
import { Page2 } from '../pages/page2/page2';
import { MagazinesPage } from '../pages/magazines/magazines';
import { InstitutionsPage } from '../pages/institutions/institutions';
import { YoutubePlaylistsPage } from '../pages/youtube-playlists/youtube-playlists';

import { RadioPlayer } from '../components/radio-player.component';
import { CarouselNews } from '../components/carousel-news.components';
import { AsambleasList } from '../components/asambleas-list.components';


@NgModule({
  declarations: [
    MyApp,
    Page1,
    Page2,
    InstitutionsPage,
    MagazinesPage,

    YoutubePlaylistsPage,
    AsambleasList,
    RadioPlayer,
    CarouselNews
    
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Page1,
    Page2,
    MagazinesPage,
    InstitutionsPage,

    YoutubePlaylistsPage,
    AsambleasList,
    RadioPlayer,
    CarouselNews
    
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
