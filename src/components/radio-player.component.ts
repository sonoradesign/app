import { Component } from '@angular/core';
import { RadioPlayerService } from '../providers/radio-player-service';
import { RadioPlayerAudioHTML5 } from '../providers/radio-player-audio-html5';

@Component({
    template: "<button ion-button (click)='Play()' icon-only end><ion-icon name='md-play'></ion-icon></button>",
    selector: "radio-player",
    providers:[RadioPlayerService, RadioPlayerAudioHTML5]
})
export class RadioPlayer {
    Played: Boolean = false;

    constructor(private rp: RadioPlayerService, private player: RadioPlayerAudioHTML5) {
    }

    Play() {
        this.player.Play();
    }
}