import { Component } from '@angular/core';
import { SNTEAPI } from '../providers/snteapi';

@Component({
    template: "<ion-slides pager> <ion-slide *ngFor='let new of news'><img src='{{new.img}}' /></ion-slide> </ion-slides>",
    selector: "carousel-news",
    providers:[SNTEAPI]
})
export class CarouselNews {
    news;
    constructor(api: SNTEAPI) {
        api.News().subscribe(
            data => this.news = data
        );
    }
}