import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import  { SNTEAPI } from '../../providers/snteapi';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
/*
  Generated class for the YoutubePlaylists page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-youtube-playlists',
  templateUrl: 'youtube-playlists.html',
  providers: [SNTEAPI, YoutubeVideoPlayer]
})
export class YoutubePlaylistsPage {
  videos:any;
  error:string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, private api: SNTEAPI, private youtube: YoutubeVideoPlayer) {
      this.api.YouTubePlayList().subscribe(
            data => this.videos = data
      );
  }


  openVideo(event, videoId) {
      try {
        console.log(this.youtube);
        this.youtube.openVideo(videoId);
      } catch(ex) {
        console.log(ex);
      }
  }
}
