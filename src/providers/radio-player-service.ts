import { Injectable } from '@angular/core';
import {StreamingMedia, StreamingVideoOptions} from 'ionic-native';
/*
  Generated class for the RadioPlayerService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/

@Injectable()
export class RadioPlayerService {
  private urlStreaming = "http://snteradio.com:9050/;";
  Played: Boolean = false;

  constructor() {
    console.log("Plugin StreamingMedia!");
  }

  Play() {
      let options: StreamingVideoOptions = {
        successCallback: () => { console.log('Video played') },
        errorCallback: (e) => { console.log('Error streaming') },
        orientation: 'landscape'
      };

      if(this.Played){
          StreamingMedia.stopAudio();
          this.Played = false;
      } else {
          StreamingMedia.playAudio(this.urlStreaming, options);
          this.Played = true;
      }
  }

}
