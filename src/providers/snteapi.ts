import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Rx';

/*
  Generated class for the Snteapi provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class SNTEAPI {
  apiURL: String = "https://ichavez:admin@sonoradesign.mx/SNTE/index.php/api";
  /*apiURL: String = "https://ichavez:admin@sonoradesign.mx/SNTE/index.php/api";*/
  response;
  Headers;

  constructor(public http: Http) {
     this.Headers = new Headers({
       "Content-Type": "application/json"
     }); 
  }

  //Endpoints

  News()  {
    return this.http.get(this.apiURL.toString() + "/news", {headers: this.Headers})
     .map( (res) => res.json())
     .catch((error:any) => Observable.throw(error.json() || 'Server error'));
  }

  YouTubePlayList() {
     return this.http.get(this.apiURL.toString() + "/youtube", {headers: this.Headers})
     .map((res) => res.json())
     .catch((error:any) => Observable.throw(error.json() || 'Server error'));
  }

  Institutions(){
    return this.http.get(this.apiURL.toString() + "/institutions", {headers: this.Headers})
    .map( (res) => res.json())
    .catch((error:any) => Observable.throw(error.json() || 'Server error'));
  }

}
