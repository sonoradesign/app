import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the RadioPlayerAudioHTML5 provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class RadioPlayerAudioHTML5 {
  private urlStreaming = "http://snteradio.com:9050/;";
  Played: Boolean = false;
  stream:any;
  promise:any;

  constructor(public http: Http) {
     this.stream = new Audio(this.urlStreaming);
  }

   Play() {
      if(!this.Played){
          this.stream.play();
          this.promise = new Promise((resolve,reject) => {
            this.stream.addEventListener('playing', () => {
              this.Played = true;
              resolve(true);
            });

            this.stream.addEventListener('error', () => {
              reject(false);
            });
          });
   
        return this.promise;
      } else {
          this.Played = false;
      }
  }

  Pause() {
    this.stream.pause();
  }
}
